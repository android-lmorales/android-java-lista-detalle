package com.lmorales.lista_detalle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<PokemonElement> elements;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    public void init(){
         elements = new ArrayList<>();
         elements.add(new PokemonElement("David"));
        elements.add(new PokemonElement("Leonardo"));
        elements.add(new PokemonElement("Evelyn"));
        elements.add(new PokemonElement("Amelia"));
        elements.add(new PokemonElement("Liam"));
        elements.add(new PokemonElement("Genoveva"));
        PokemonAdapter pokemonAdapter = new PokemonAdapter(elements,this);
        RecyclerView recyclerView = findViewById(R.id.listRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(pokemonAdapter);
    }
}