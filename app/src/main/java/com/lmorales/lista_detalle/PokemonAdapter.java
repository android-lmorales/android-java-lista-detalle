package com.lmorales.lista_detalle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> {

    //creamos listado de Pokemon
    private List<PokemonElement> PokemonData;
    // usamos inflater para indicar que view se utilizara
    private LayoutInflater mInflater;
    //
    private Context context;

    public PokemonAdapter(List<PokemonElement> pokemonList, Context context){
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.PokemonData = pokemonList;
    }

    @Override
    public int getItemCount(){return PokemonData.size();}

    @Override
    public PokemonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        //damos la referencia del layout q utilizaremos
        View view = mInflater.inflate(R.layout.recycler_view_item,null);

        return new PokemonAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final PokemonAdapter.ViewHolder holder, final int position){
        holder.bindData(PokemonData.get(position));
    }

    public void setItems(List<PokemonElement> items){ PokemonData = items;}

    public class ViewHolder extends RecyclerView.ViewHolder{
    TextView name;

    ViewHolder(View itemView){
        super(itemView);
        name= itemView.findViewById(R.id.textView1);
    }

    void bindData(final PokemonElement item){
        name.setText(item.getName());
    }
    }
}
