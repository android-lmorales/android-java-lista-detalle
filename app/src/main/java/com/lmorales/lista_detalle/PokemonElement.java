package com.lmorales.lista_detalle;

public class PokemonElement {
    String name;

    public PokemonElement() {
    }

    public PokemonElement(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
